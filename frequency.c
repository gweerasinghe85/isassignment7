#include<stdio.h>
#define MAXSIZE 100

int main()
{
	int i,count=0;
	char ch;
	char input[MAXSIZE];
	printf("Enter the sentence: ");
	fgets(input,MAXSIZE,stdin);
	printf("Enter the character: ");
	scanf("%c",&ch);

	for(i=0;input[i]!='\0';i++)
	{
		if (input[i]==ch)
			count=count+1;
	}
	printf("The frequency of the letter %c is %d.",ch,count);
	return 0;
}
