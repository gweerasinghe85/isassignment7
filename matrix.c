#include <stdio.h>
void input();
void add();
void multiply();
void answer();

void input(int x[50][50], int y[50][50], int rowA, int colA, int rowB, int colB)
{
  printf("\nEnter elements to the matrix A:\n");
  for(int i=0; i<rowA; i++)
    {
    for(int j=0; j<colA; j++)
    {
      printf("Input Number [%d, %d]: ", i+1, j+1);
      scanf("%d",&x[i][j]);
    }
    }
  printf("\nEnter elements to the matrix B:\n");
  for(int i=0; i<rowB; i++)
    {
    for(int j=0; j<colB; j++)
    {
      printf("Input Number [%d, %d]: ", i+1, j+1);
      scanf("%d", &y[i][j]);
  }
  }
}
void add(int x[50][50],int y[50][50],int rowA,int colA)
{
  int ad[50][50];
  for(int i=0;i<rowA;i++)
    {
    for(int j=0;j<colA;j++)
    {
       ad[i][j]=x[i][j]+y[i][j];
  }
  }
  printf("\nAddition (Matrix A + Matrix B):\n");
  answer(ad,rowA,colA);
}
void multiply(int x[50][50],int y[50][50],int rowA,int colA,int colB)
{
  int mul[50][50];
  for(int i=0;i<rowA;i++)
    {
    for(int j=0;j<colB;j++)
    {
      mul[i][j]=0;
      for(int k=0;k<colA;k++)
        {
        mul[i][j]=mul[i][j]+(x[i][k]*y[k][j]);
        }
    }
    }
   printf("\nMultiplication (Matrix A * Matrix B):\n");
   answer(mul,rowA,colB);
}
void answer(int x[50][50],int r,int c)
{
  for(int i=0;i<r;i++)
    {
    for(int j=0;j<c;j++)
    {
      printf("%d\t",x[i][j]);
    }
    printf("\n");
    }
}

int main(){
  int x[50][50], y[50][50];
  int rowA,rowB,colA,colB;
  printf("Matrix A dimensions\n");
  printf("Enter the number of rows: ");
  scanf("%d",&rowA);
  printf("Enter the number of columns: ");
  scanf("%d",&colA);
  printf("\nMatrix B dimensions\n");
  printf("Enter the number of rows: ");
  scanf("%d",&rowB);
  printf("Enter the number of columns: ");
  scanf("%d",&colB);
  input(x, y, rowA, colA, rowB, colB);
  if(!((rowA==rowB)&&(colA==colB))) printf("\nCannot perform addition due to a mathematical error!\n");
  else add(x,y,rowA,colB);
  if(!(colA==rowB)) printf("\nCannot perform multiplication due to a mathematical error!\n");
  else multiply(x,y,rowA,colA,colB);
  return 0;
}
